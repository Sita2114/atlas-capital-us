/* eslint-disable no-console */

(function ($) {
      'use strict';
      /**
       * Global Variables
       */
      const isMobile = {
        Android: function () {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
          return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
          return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
          );
        },
      };
      /**
       * Returns a function, that, as long as it continues to be invoked, will not
       * be triggered. The function will be called after it stops being called for
       * N milliseconds. If `immediate` is passed, trigger the function on the
       * leading edge, instead of the trailing.
       * @param {fn} func - function to debounce
       * @param {number} wait - time to wait
       * @param {bool} immediate
       * @returns {Function}
       */
      const debounce = function (func, wait, immediate) {
        let timeout;
        let waitTime = wait || 100;
        return function () {
          let context = this,
            args = arguments;
          clearTimeout(timeout);
          timeout = setTimeout(function () {
            timeout = null;
            if (!immediate) {
              func.apply(context, args);
            }
          }, waitTime);
          if (immediate && !timeout) {
            func.apply(context, args);
          }
        };
      };
      /**
       * Collection of useful site functions
       * @type {{init: init, smoothScroll: smoothScroll}}
       */
      const siteFunctions = {
        init: function () {
          siteFunctions.smoothScroll();
        },
        /**
         * Smooth Scroll function for anchor clicks
         */
        smoothScroll: function () {
          $('a[href*="#"]').click(function () {
            let target = $(this.hash);
            if (
              location.pathname.replace(/^\//, '') ===
                this.pathname.replace(/^\//, '') &&
              location.hostname === this.hostname
            ) {
              target = target.length
                ? target
                : $('[name=' + this.hash.slice(1) + ']');
              if (target.length) {
                $('html, body')
                  .stop()
                  .animate(
                    {
                      scrollTop: target.offset().top - 75,
                    },
                    1000
                  );
                return false;
              }
            }
          });
        },
      };

      const siteHeader = {
        init: function () {},
        /**
         * Handle on scroll header functionality
         */
        scrollChange: function () {
          let $body = $('body');
          $(document).scrollTop() > 50
            ? $body.addClass('scroll')
            : $body.removeClass('scroll'); //jshint ignore:line
        },
      };

      const siteMenu = {
        init: function () {
          $('.js-hamburger').click(function () {
            siteMenu.toggle();
          });
        },

        toggle: function () {
          $('.a-hamburger, .m-list.-primary').toggleClass('-active');
        },
      };

      const sticky = {
        init: function () {
          sticky.toggleClass();
        },

        toggleClass: function () {
          const header = document.querySelector('.js-header');

          if (window.pageYOffset > 0) {
            header.classList.add('-sticky');
          } else {
            header.classList.remove('-sticky');
          }
        },
      };

      const slider = {
        init: () => {
          slider.slider();
        },

        slider: () => {
          const swiper = new Swiper('.js-logoSlider', {
            // Optional parameters
            loop: true,
            slidesPerView: 1,

            breakpoints: {
              991: {
                slidesPerView: 3,
              },

              1200: {
                slidesPerView: 5,
              },
            },

            // If we need pagination
            pagination: {
              el: '.js-logoSliderPagination',
              clickable: true,
            },

            // Navigation arrows
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
          });
        },
      };

      const parallax = {
        init: () => {
          parallax.parallax();
        },

        parallax: () => {
          // Iterate elements above the selected one
          const getOffsetTop = (element) => {
            let offsetTop = 0;
            while (element) {
              offsetTop += element.offsetTop;
              element = element.offsetParent;
            }
            return offsetTop;
          };

          let el = document.querySelector('.js-teamParallax');
          // This is the selected element. getOffsetTop returns true position
          const elPosition = getOffsetTop(el);
          // How much we scrolled on page
          let scrolled = window.pageYOffset;
          // Difference between elPosition and scrolled will give us parallax
          let rateScrolled = (elPosition - scrolled) * 0.5;

          el.style.transform = `translateY(${rateScrolled}px`;
        },
      };

      const disableLink = {
        init: () => {
          disableLink.disable();
        },

        disable: () => {
          const links = document.querySelectorAll('a');

          links.forEach((item) => {

            if (item.attributes.href.value === '#') {

              item.addEventListener('click', (event) => {
                event.preventDefault();
              });
            }
          });
        },
      };

      const hrefToId = {

        init: () => {
          hrefToId.transpose();
        },

        transpose: () => {
          const el = document.querySelectorAll('.js-primaryList a');

          el.forEach(item => {

            if(item.attributes.href.value) {
              const href = item.attributes.href.value;

              item.id = href;
            }
          });
        }
      };

      const modal = {

        init: () => {
          modal.toggle();
        },

        toggle: () => {
          const el = document.querySelectorAll('.js-modals > *');

          el.forEach(item => {

            item.addEventListener('click', () => {
              const body = document.querySelector('body');
              const currentModal = item.parentElement.parentElement.querySelector('.js-modal');
              const closeElements = currentModal.parentElement.querySelectorAll('.js-modal, .js-close');
              // console.log(closeElements);
              // console.log(currentModal);
              currentModal.classList.add('-open');
              body.classList.add('-scrollY0');

              closeElements.forEach(closeEl => {

                closeEl.addEventListener('click', () => {
                  currentModal.classList.remove('-open');
                  body.classList.remove('-scrollY0');
                });
              });
            });
          });
        }
      };

      const scrollspy = {

        init: () => {
          scrollspy.spy();
        },

        spy: () => {
            'use strict';

            var section = document.querySelectorAll('.js-scrollspy');
            var sections = {};
            var i = 0;

            Array.prototype.forEach.call(section, function(e) {
              sections[e.id] = e.offsetTop;
            });

            window.onscroll = function() {
              var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;

              for (i in sections) {
                if (sections[i] <= scrollPosition) {
                  document.querySelector('.-active').setAttribute('class', ' ');
                  document.querySelector('a[href*=' + i + ']').setAttribute('class', '-active');
                }
              }
            };
        }
      };

      $(document).ready(function () {
        siteFunctions.init();
        siteMenu.init();
        slider.init();
        disableLink.init();
        hrefToId.init();
        modal.init();
        scrollspy.init();
        AOS.init();

        $(window).scroll(
          debounce(function () {
            siteHeader.scrollChange();
            sticky.init();
          })
        );

        // With no debounce
        $(window).scroll(() => {
          parallax.init();
        });
      });

      $(window).on('load', function () {});
})(jQuery);
