<?php
/* Template Name: Home */
get_header(); ?>
    <div id="content" class="site-content">
        <main id="main" class="page-main site-main" role="main">
            <?php
                $background_image = get_field('hero_background_image');
                $image = get_field('hero_logo');
                $heading = get_field('hero_heading');
            ?>
            <section class="m-hero" style="background-image: url('<?php if($background_image): echo $background_image['url']; endif; ?>');">
                <div class="_wr">
                    <div class="_w">
                        <?php if($image): ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        <?php endif; ?>
                        <h2 class="m-hero__heading"><?php if($heading): echo $heading; endif; ?></h2>
                    </div>
                </div>
            </section>
            <?php
                $background_image = get_field('why_now_background_image');
            ?>
            <section class="m-section -shapes -pb200 js-scrollspy" id="why-now" style="background-image: url('<?php if($background_image): echo $background_image['url']; endif; ?>');">
                <div class="_wr">
                    <div class="_w">
                        <?php
                            $heading = get_field('why_now_heading');
                            $first_text = get_field('why_now_first_text');
                            $second_text = get_field('why_now_second_text');
                        ?>
                        <section class="m-section__sections">
                            <h2 class="m-section__sections--heading"><?php if($heading): echo $heading; endif; ?></h2>
                            <p class="m-section__sections--text"><?php if($first_text): echo $first_text; endif; ?></p>
                            <p class="m-section__sections--text -normal"><?php if($second_text): echo $second_text; endif; ?></p>
                        </section>
                        <?php
                            $heading = get_field('our_approach_heading');
                            $text = get_field('our_approach_text');
                        ?>
                        <section class="m-section__sections">
                            <h2 id="our-approach" class="m-section__sections--heading js-scrollspy"><?php if($heading): echo $heading; endif; ?></h2>
                            <p class="m-section__sections--text"><?php if($text): echo $text; endif; ?></p>
                        </section>
                        <?php
                            $heading = get_field('mission_heading');
                            $text = get_field('mission_text');
                        ?>
                        <h2 class="m-section__sections--heading -small"><?php if($heading): echo $heading; endif; ?></h2>
                        <p class="m-section__sections--text -normal"><?php if($text): echo $text; endif; ?></p>
                    </div>
                </div>
            </section>
            <?php
                $background_image = get_field('atlas_family_background_image');
                $heading = get_field('atlas_family_heading');
                $sub_heading = get_field('atlas_family_sub_heading');
                $first_text = get_field('atlas_family_first_text');
                $second_text = get_field('atlas_family_second_text');
            ?>
            <section class="m-section -country js-scrollspy" id="culture" style="background-image: url('<?php if($background_image): echo $background_image['url']; endif; ?>');">
                <div class="_wr">
                    <div class="_w">
                        <section class="m-section__sections -pt0 -pr30">
                            <h2 class="m-section__sections--heading"><?php if ($heading): echo $heading; endif; ?></h2>
                            <h2 class="m-section__sections--subHeading"><?php if ($sub_heading): echo $sub_heading; endif; ?></h2>
                            <p class="m-section__sections--text -normal"><?php if ($first_text): echo $first_text; endif; ?></p>
                            <p class="m-section__sections--text -normal"><?php if ($second_text): echo $second_text; endif; ?></p>
                        </section>
                    </div>
                </div>
            </section>
            <section class="m-logos">
                <div class="_wr">
                    <div class="_w">
                        <div class="m-slider -logos js-logoSlider">
                            <div class="swiper-wrapper">
                                <?php if( have_rows('logo_slider') ): ?>
                                    <?php while( have_rows('logo_slider') ): the_row(); ?>
                                        <?php
                                            $logo = get_sub_field('logo_slider_image');
                                        ?>
                                        <img src="<?php if($logo): echo $logo['url']; endif; ?>" alt="<?php if($logo): echo $logo['alt']; endif; ?>" class="swiper-slide">
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <div class="m-slider__pagination js-logoSliderPagination"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="m-section -team">
                <div class="_wr">
                    <div class="_w">
                        <?php
                            $heading = get_field('atlas_team_heading');
                            $text = get_field('atlas_team_text');
                        ?>
                        <h2 class="m-section__heading"><?php if($heading): echo $heading; endif; ?></h2>
                        <p class="m-section__text"><?php if($text): echo $text; endif; ?></p>
                    </div>
                </div>
            </section>
            <section class="m-team -team js-scrollspy" id="team">
                <div class="_wr">
                    <div class="_w">
                        <?php if( have_rows('atlas_team_team') ): ?>
                            <?php while( have_rows('atlas_team_team') ): the_row(); ?>
                                <?php
                                    $image = get_sub_field('atlas_team_team_image');
                                    $name = get_sub_field('atlas_team_team_name');
                                    $title = get_sub_field('atlas_team_team_title');
                                    $modal_text = get_sub_field('atlas_team_modal_text');
                                ?>
                                <div class="_l4 m-team__column" data-aos="fade-left">
                                    <div class="m-team__content js-modals">
                                        <?php if($image): ?>
                                        <img class="m-team__content--image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                        <?php endif; ?>
                                        <h2 class="m-team__content--heading"><?php if($name): echo $name; endif; ?></h2>
                                        <p class="m-team__content--text"><?php if($title): echo $title; endif; ?></p>
                                    </div>
                                    <div class="m-modal js-modal -team">
                                        <div class="m-modal__content">
                                            <?php if($image): ?>
                                            <img class="m-modal__content--image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                            <?php endif; ?>
                                            <h2 class="m-modal__content--heading"><?php if($name): echo $name; endif; ?></h2>
                                            <div class="a-icons -line">
                                                <span class="a-icons -chevron"></span>
                                            </div>
                                            <h3 class="m-modal__content--subHeading"><?php if($title): echo $title; endif; ?></h3>
                                            <p class="m-modal__content--text"><?php if($modal_text): echo $modal_text; endif; ?></p>
                                            <span class="a-icons -close js-close"></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            <section class="m-team -investment">
                <div class="_wr">
                    <div class="_w">
                        <?php $heading = get_field('investment_heading'); ?>
                        <h2 class="m-team__heading" data-aos="fade-left"><?php if($heading): echo $heading; endif; ?></h2>
                        <?php if( have_rows('investment_committee') ): ?>
                            <?php while( have_rows('investment_committee') ): the_row(); ?>
                                <?php
                                    $image = get_sub_field('investment_committee_image');
                                    $name = get_sub_field('investment_committee_name');
                                    $title = get_sub_field('investment_committee_title');
                                    $modal_text = get_sub_field('investment_comittee_modal_text');
                                ?>
                                <div class="_l4 m-team__column" data-aos="fade-left">
                                    <div class="m-team__content js-modals">
                                        <?php if($image): ?>
                                        <img class="m-team__content--image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                        <?php endif; ?>
                                        <h2 class="m-team__content--heading"><?php if($name): echo $name; endif; ?></h2>
                                    </div>
                                    <div class="m-modal js-modal -investment">
                                        <div class="m-modal__content">
                                            <?php if($image): ?>
                                            <img class="m-modal__content--image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                            <?php endif; ?>
                                            <h2 class="m-modal__content--heading"><?php if($name): echo $name; endif; ?></h2>
                                            <div class="a-icons -line">
                                                <span class="a-icons -chevron"></span>
                                            </div>
                                            <h3 class="m-modal__content--subHeading"><?php if($title): echo $title; endif; ?></h3>
                                            <p class="m-modal__content--text"><?php if($modal_text): echo $modal_text; endif; ?></p>
                                            <span class="a-icons -close js-close"></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            <?php
                $image = get_field('join_our_team_background_image');
                $first_heading = get_field('join_our_team_first_heading');
                $first_text = get_field('join_our_team_first_text');
                $second_heading = get_field('join_our_team_second_heading');
                $second_text = get_field('join_our_team_second_text');
                $button = get_field('join_our_team_button');
            ?>
            <section class="m-twoSection -team" style="background-image: url('<?php if($image): echo $image['url']; endif; ?>');">
                <div class="_wr">
                    <div class="_w">
                        <div class="_l6 m-twoSection__column -first">
                            <div class="m-twoSection__content">
                                <h2 class="m-twoSection__content--heading"><?php if($first_heading): echo $first_heading; endif; ?></h2>
                                <p class="m-twoSection__content--text"><?php if($first_text): echo $first_text; endif; ?></p>
                                <?php if($button): ?>
                                <a href="<?php echo $button['url']; ?>" class="m-twoSection__content--link a-button -main -team">
                                    <?php echo $button['title']; ?>
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="_l6 m-twoSection__column -second">
                            <div class="m-twoSection__content js-teamParallax">
                                <h2 class="m-twoSection__content--heading"><?php if($second_heading): echo $second_heading; endif; ?></h2>
                                <p class="m-twoSection__content--text"><?php if($second_text): echo $second_text; endif; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="m-contact js-scrollspy" id="contact">
                <div class="_wr">
                    <div class="_w">
                        <div class="m-contact__content">
                            <ul class="m-list -contact">
                                <?php if ( have_rows('contact_link_list') ): ?>
                                    <?php while ( have_rows('contact_link_list') ): the_row(); ?>
                                        <?php
                                            $text = get_sub_field('contact_link_list_text');
                                            $link = get_sub_field('contact_link_list_link');
                                        ?>
                                        <li class="m-list__item">
                                            <?php if ($link): ?>
                                            <a href="<?php echo $link['url']; ?>">
                                                <?php echo $link['title']; ?>
                                            </a>
                                            <?php endif; ?>
                                        </li>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="m-info">
                            <div class="_wr">
                                <div class="_w -aic">
                                    <?php
                                        $email = get_field('contact_email');
                                        $content = get_field('contact_content', false, false);
                                    ?>
                                    <div class="_l6">
                                        <div class="m-info__content">
                                            <?php if ($email): ?>
                                            <a href="<?php echo 'mailto:' . $email; ?>">
                                                <?php echo 'EMAIL: ' . $email; ?>
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="_l6">
                                        <div class="m-info__content -wysiwyg">
                                            <?php if($content): ?>
                                                <?php echo $content; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php
                $background_image = get_field('copyright_background_image');
                $text = get_field('copyright_text');
            ?>
            <section class="m-copyright" style="background-image: url('<?php if($background_image): echo $background_image['url']; endif; ?>');">
                <div class="_wr">
                    <div class="_w">
                        <div class="m-copyright__content">
                            <span>
                                <?php echo '© ' . date('Y') . ' ' . $text; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
<?php
get_footer();