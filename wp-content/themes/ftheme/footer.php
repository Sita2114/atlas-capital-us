<?php
/**
 * @package ftheme
 */
?>
<footer id="colophon" class="m-footer" role="contentinfo">
    <div class="_wr">
        <div class="_w">
            <?php $content = get_field('footer_content', false, false); ?>
            <div class="m-footer__content">
                <p>
                    <?php if($content): echo $content; endif; ?>
                </p>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
