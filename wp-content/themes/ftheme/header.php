<?php
/**
 * @package ftheme
 */
global $globalSite;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    
     <?php if(has_site_icon()): ?>
        <link rel="icon" href="<?php echo get_site_icon_url(); ?>" type="image/x-icon" />
    <?php else: ?>
        <link rel="icon" href="<?php echo get_template_directory_uri() . '/bundles/images/favicon.ico'; ?>" type="image/x-icon" />
    <?php endif; ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <div class="m-overlay"></div>
    <header id="masthead" class="m-header js-header" role="banner">
        <div class="_wr-f">
            <div class="_w -aic -jcsb">
                <?php
                    $logo = get_field('site_logo', 'options');
                ?>
                <a href="<?php _e( home_url( '/' ) ); ?>" id="brand" class="site-logo -active">
                    <img class="a-logo" src="<?php if($logo): echo $logo; endif; ?>" />
                </a>
                <div class="js-hamburger a-hamburger -mla">
                    <div>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <nav id="site-navigation" class="m-navbar" role="navigation">
                    <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu', 'menu_class' => 'm-list -primary js-primaryList' ) ); ?>
                </nav>
            </div>
        </div>
    </header>